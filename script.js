Теоретичні питання
1. Що таке оператори в JavaScript і які їхні типи ви знаєте ?
    
    Оператори — спеціальні символи або команди, які повідомляють що треба виконати певну дію
    Оператори бувають унарні(працюють з 1єю змінною) та бінарні(працюють з 2ма змінними або більше)
    Оператори є арефметичні, логічні та оператори порівняння

    
2. Для чого використовуються оператори порівняння в JavaScript? Наведіть приклади таких операторів.

    Оператори порівняння використовуються для порівняння 2ох змінних.Та повертає булеве значеення в залежності
    від результу порівняння.

    Приклади таких операторів: >, <, ==, ===, !=, !==

3. Що таке операції присвоєння в JavaScript? Наведіть кілька прикладів операцій присвоєння.

    Операції присвоєння — запис значення у змінну

    const number = 10;
    const text = '10';
    
    let 

// Практичні завдання

/* 1. Створіть змінну "username" і присвойте їй ваше ім'я. Створіть змінну "password" і присвойте їй пароль
    (наприклад, "secret").Далі ми імітуємо введеня паролю користувачем.Отримайте від користувача значення
    його паролю і перевірте, чи співпадає воно зі значенням в змінній "password".Виведіть результат порівнння
    в консоль. */


const userName = 'Vladyslav';
const password = 'js63472';

let login = prompt(`Hello ${userName}! Enter your password`);

if (login === password) {
    alert("Пароль введено вірно");
}
if (login !== password) {
    alert("Пароль введено невірно");
}

console.log(login === password);


/*2. Створіть змінну x та присвойте їй значення 5. Створіть ще одну змінну y
    та запишіть присвойте їй значення 3.
    Виведіть результати додавання, віднімання, множення та ділення
    змінних x та y у вікні alert.*/

const x = 5;
const y = 3;

alert(x + y);
alert(x - y);
alert(x * y);
alert(x / y);



